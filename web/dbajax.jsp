<%-- 
    Document   : dbajax
    Created on : 29 Apr, 2020, 10:34:17 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jquery-3.5.0.min.js"></script>
        <script src="dbajax.js"> </script>
    </head>
    <body>
        <h1>DICTIONARY!!</h1>
        <hr>
        Enter the Word:
        <br>
        <input type="text" name="ip" id="ip">
        <input type="button" value="search" onclick="loaddata()"><br>
        Meaning:<br>
        <div id="dbdata"></div>
    </body>
</html>
